<?php

require_once 'lib/Twocheckout.php';
require_once './config.php';

Twocheckout::privateKey($config['privateKey']); //Private Key
Twocheckout::sellerId($config['sellerId']); // 2Checkout Account Number
Twocheckout::sandbox($config['sandbox']); // Set to false for production accounts.
Twocheckout::verifySSL($config['ssl']);

try {
    $charge = Twocheckout_Charge::auth([
        'merchantOrderId' => '123',
        'token' => $_POST['token'],
        'currency' => 'USD',
        'total' => '1.00',
        'lang' => 'es',
        // "lineItems" => array (
        // "0" => array(
        // "type" => "product",
        // "name" => "test recurring",
        // "quantity" => "1",
        // "price" => "1.00",
        // "recurrence" => "1 Week",
        // "duration" => "Forever"
        // )
        // ),
        'billingAddr' => [
            'name' => 'John Doe',
            'addrLine1' => 'Test',
            'city' => 'Bucharest',
            'state' => 'Georgia',
            'zipCode' => '10001',
            'country' => 'US',
            'email' => 'test@2checkout.com',
            'phoneNumber' => '0000000',
        ],
    ]);

    $wasSuccessful = 'APPROVED' == $charge['response']['responseCode'];
    $type = $wasSuccessful ? 'succes' : 'failure';
    $message = $wasSuccessful ? 'Payment sucessful!' : 'Unable to process payment!';

    echo json_encode(
        [
            $type => [
                'message' => $message,
                'checkout' => json_encode($charge),
            ],
        ]
    );
} catch (Twocheckout_Error $e) {
    echo json_encode(
        [
            'error' => [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'code' => $e->getCode(),
            ],
        ]
    );
}
